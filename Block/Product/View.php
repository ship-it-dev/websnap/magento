<?php


namespace Websnap\Magento\Block\Product;


use Magento\Catalog\Api\ProductRepositoryInterface;
use Websnap\Magento\Model\Config;

class View extends \Magento\Catalog\Block\Product\View
{
    /**
     * @var Config
     */
    private $moduleConfig;

    public function __construct(
        Config $moduleConfig,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );

        $this->moduleConfig = $moduleConfig;
    }

    protected function _toHtml()
    {

        if (!$this->moduleConfig->getToken()) {
            return '';
        }

        return parent::_toHtml();
    }
}
