## Installation
1. Install with composer: `composer require websnap/magento`
2. Run `bin/magento setup:upgrade`

## Getting started
1. Register for free at https://www.websnap.app and obtain a token
2. Add your token in the websnap Magento configuration section

## Usage
The module will add a new `og:image` tag which returns a live screenshot of the `websnap/product/view` route.

You can bypass the screenshot and force a html response for local development.\
Just enable the 'Bypass' flag in the websnap configuration section.

We tried to create an image template which fits the basic Magento theme. If this doesn't match your design, you can adjust the template by any means provided by Magento. 

## Contact

Did you found a bug or need a certain feature? Please get in [touch](mailto:support@websnap.app) with us or [raise an issue](https://gitlab.com/ship-it-dev/websnap/magento/-/issues/new).
