<?php


namespace Websnap\Magento\Controller\Product;


use Magento\Catalog\Controller\Product;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NotFoundException;

class View extends Product
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $pageFactory;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        Context $context
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }


    public function execute()
    {
        $product = $this->_initProduct();

        if (!$product) {
            throw new NotFoundException(__('Could not find product'));
        }

        $page = $this->pageFactory->create();
        $page->getConfig()->getTitle()->set($product->getName());

        return $page;
    }
}
