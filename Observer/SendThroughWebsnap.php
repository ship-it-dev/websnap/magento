<?php


namespace Websnap\Magento\Observer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Response\Http as HttpResponse;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\UrlInterface;
use Psr\Http\Message\ResponseInterface;
use Websnap\Magento\Model\Config;
use Websnap\Php\ClientFactory;

class SendThroughWebsnap implements ObserverInterface
{
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var ClientFactory
     */
    private $clientFactory;
    /**
     * @var Config
     */
    private $moduleConfig;
    /**
     * @var array
     */
    private $mappings;

    public function __construct(
        Config $moduleConfig,
        ClientFactory $clientFactory,
        UrlInterface $url,
        array $mappings = []
    )
    {
        $this->url           = $url;
        $this->clientFactory = $clientFactory;
        $this->moduleConfig  = $moduleConfig;
        $this->mappings      = $mappings;
    }


    // TODO - cleanup method
    public function execute(Observer $observer)
    {

        if ($this->moduleConfig->isBypassEnabled()) {
            return;
        }

        $request = $observer->getData('request');
        $action  = $observer->getData('controller_action');

        if (
            !$request instanceof Http ||
            !$action instanceof Action
        ) {
            return;
        }

        $fullActionName = $request->getFullActionName();
        if (!array_key_exists($fullActionName, $this->mappings)) {
            return;
        }

        if ($request->getHeader('X-Websnap-Version')) {
            return;
        }

        $this->markDispatched($action, $request);

        $websnap = $this->makeScreenshot($request);

        $this->convertWebsnapToResponse(
            $action->getResponse(),
            $websnap
        );

    }

    private function makeScreenshot(Http $request): ResponseInterface
    {
        $client = $this->clientFactory->create([
            'token' => $this->moduleConfig->getToken()
        ]);

        $options = array_merge(
            $this->mappings[$request->getFullActionName()],
            $request->getParam('websnap', [])
        );

        return $client->screenshot(
            $this->getCurrentUrl(),
            $options
        );
    }

    private function markDispatched(Action $action, Http $request): void
    {
        $action->getActionFlag()->set('', Action::FLAG_NO_DISPATCH, true);
        $request->setDispatched();
    }

    private function convertWebsnapToResponse(HttpResponse $response, ResponseInterface $websnap): void
    {
        $headers = [
            'Content-Type',
            'Content-Length',
            'Cache-Control'
        ];

        $response->setContent($websnap->getBody()->getContents());

        foreach ($headers as $header) {
            $value = $websnap->getHeaderLine($header);

            $response->setHeader(
                $header,
                $value
            );
        }

        $response->send();
    }

    private function getCurrentUrl(): string
    {
        $url = $this->url->getCurrentUrl();

        $this->applyCacheTag($url);

        return $url;

    }

    private function applyCacheTag(string &$url): void
    {
        $glue      = '?';
        $fragments = explode($glue, $url);
        $params    = [];

        if (count($fragments) > 1) {
            parse_str($fragments[1], $params);
        }

        $params['websnap'] = array_merge(
            $params['websnap'] ?? [],
            [
                'cache_tag' => 'websnap'
            ]
        );

        $fragments[1] = http_build_query($params);
        $url          = implode($glue, $fragments);
    }
}
