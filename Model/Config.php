<?php


namespace Websnap\Magento\Model;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Config
{
    public const DEFAULT_VIEWPORT_WIDTH = 1200;
    public const DEFAULT_VIEWPORT_HEIGHT = 630;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig  = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    public function getToken(): string
    {
        return $this->scopeConfig->getValue(
            'websnap/general/token',
            ScopeInterface::SCOPE_STORES,
            $this->storeManager->getStore()->getId()
        ) ?: '';
    }

    public function isBypassEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            'websnap/dev/bypass',
            ScopeInterface::SCOPE_STORES,
            $this->storeManager->getStore()->getId()
        );
    }

}
